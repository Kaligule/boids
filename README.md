# Boids

Boids trying not to collide with each other.
The parameters of the individuals do evolve over time, hopefully making them better.
There are statistics logged into the browsers console.

## See them in action

- **local**: open the file [index.html](index.html)
- **online**: visit the latest version on main on [gitlab pages](https://kaligule.gitlab.io/boids)

# License

Based on the [code of Ben Eater](https://eater.net/boids) which is [MIT licensed](http://en.wikipedia.org/wiki/MIT_License).

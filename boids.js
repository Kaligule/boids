// GLOBAL CONSTANTS

const NUM_BOIDS = 30;
const COLLISION_DISTANCE = 5;

const MARGIN = 200;
const PARENT_FUNCTION = (boid => boid.age * boid.age + magnitude(boid.velocity) * 100)
const MAX_MUTATION_ABSOLUTE = 0.5 // the originial value will be changed by at most so much

const DRAW_TRAIL = true;
const DRAW_VISUAL_RANGE = false;
const DRAW_MIN_DISTANCE = true;

const BOID_COLOR = "cornflowerblue";
const TRAIL_COLOR = "red";
const VISUAL_RANGE_COLOR = "grey";
const MIN_DISTANCE_COLOR = "white";
const COLLISION_COLOR = "firebrick";
const SOFTBORDER_COLOR = "orange";

const PROPERTIES_TO_TRACK = [
  "age",
  "velocity magnitude",
  "acceleration magnitude",
  "minDistance",
  "centeringFactor",
  "avoidBoidsFactor",
  "matchingFactor",
  "avoidBorderFactor"
];

// GLOBAL VARIABLES

// Size of canvas. These get updated to fill the whole browser.
let width = 150;
let height = 150;

let play = true;

let boids = [];

let statistics = {
  allDeaths: 0,
  globalAge: 0
};

// VECTORS CALCULUS

function add_vectors(v1, v2) {
  return [
    v1[0] + v2[0],
    v1[1] + v2[1]
  ];
}

function substract_vectors(v1, v2) {
  return [
    v1[0] - v2[0],
    v1[1] - v2[1]
  ];
}

function add_list_of_vectors(list) {
  return list.reduce(add_vectors, [0,0]);
}

function magnitude(v) {
  return Math.sqrt(v[0] * v[0] + v[1] * v[1]);
}

function scale_vector(v, n) {
  return [
    n * v[0],
    n * v[1]
  ];
}

// SPAWNING BOIDS

function repopulate(random = false) {
  let parent = null;
  while (boids.length < NUM_BOIDS) {
    if (!random) {
      parent = pickRandomBoid(PARENT_FUNCTION);
    }
    boids.push(newBoid(parent));
  }
}

// random boid, but with propabilties weighted by a function
// (equal distribution by default)
function pickRandomBoid(wheight_function = (boid => 1)){
  let weights = boids.map(wheight_function);

  for (i = 0; i < weights.length; i++)
    weights[i] += weights[i - 1] || 0;

  let random = Math.random() * weights[weights.length - 1];

  for (i = 0; i < weights.length; i++)
    if (weights[i] > random)
      break;

  return boids[i];
}

function newBoid(parent) {
  return {
    age: 0,
    position: [
      Math.random() * width,
      Math.random() * height
    ],
    velocity: [
      Math.random() * 10 - 5,
      Math.random() * 10 - 5
    ],
    acceleration: [0, 0],
    history: [],
    mass: 10.,
    visualRange: 100,
    // The distance to stay away from other boids
    minDistance:       parent ? mutate(parent.minDistance,        0, 100) : 0 ,
    // adjust velocity by this %
    centeringFactor:   parent ? mutate(parent.centeringFactor,   -1,   1) : 0.,
    // Adjust velocity by this %
    avoidBoidsFactor:  parent ? mutate(parent.avoidBoidsFactor,  -1,   1) : 0.,
    // Adjust by this % of average velocity
    matchingFactor:    parent ? mutate(parent.matchingFactor,    -1,   1) : 0.,
    // Avoid window boarders this much
    avoidBorderFactor: parent ? mutate(parent.avoidBorderFactor, -5,   5) : 0.,
  };
}

function mutate(originalValue, minimalValue, maximalValue){
  let mutatedValue = originalValue + MAX_MUTATION_ABSOLUTE * 0.5 * (-1 + Math.random() + Math.random());
  return Math.min(Math.max(mutatedValue, minimalValue), maximalValue); // clamp result
}

// BOID BRAIN AND PHYSICS

function distance(boid1, boid2) {
  return magnitude(substract_vectors(boid1.position, boid2.position));
}

function distanceToBoarders(boid) {
  return Math.min(
    boid.position[0],
    width - boid.position[0],
    boid.position[1],
    height - boid.position[1]
  );
}

function boidsCloserThan(boid, minDistance) {
  return boids.filter(
    (otherBoid) => distance(boid, otherBoid) < minDistance
  ).filter(
    (otherBoid) => otherBoid != boid
  );
}

// Constrain a boid to within the window. If it gets too close to an edge,
// nudge it back in and reverse its direction.
function avoidWindowBorder(boid) {
  let awayFromBorder = [0,0];

  // left
  if (boid.position[0] < MARGIN) {
    awayFromBorder[0] =  1;
  // right
  } else if (boid.position[0] > width - MARGIN) {
    awayFromBorder[0] = -1;
  }
  // up
  if (boid.position[1] < MARGIN) {
    awayFromBorder[1] += 1;
  // down
  } if (boid.position[1] > height - MARGIN) {
    awayFromBorder[1] -= 1;
  }
  return awayFromBorder
}

// Find the center of mass of nearby boids and fly in that direction
function flyTowardsCenterOfNeighbours(boid) {
  let neighbours = boidsCloserThan(boid, boid.visualRange);
  if (neighbours.length) {
    let center_absolute = scale_vector( add_list_of_vectors(neighbours.map(b => b.position)), 1/neighbours.length);
    let center_relative = substract_vectors(center_absolute, boid.position);
    return center_relative
  }
  return [0,0]
}

// Move away from other boids that are too close to avoid colliding
function avoidCollisions(boid) {
  let neighbours = boidsCloserThan(boid, boid.minDistance);

  if (neighbours.length) {
    let sum_neighbour_positions = add_list_of_vectors(neighbours.map(b => b.position));
    let avg_neighbour_position = scale_vector(sum_neighbour_positions, 1/neighbours.length);
    let direction_away_from_others = substract_vectors( boid.position, avg_neighbour_position);
    return direction_away_from_others;
  }
  return [0,0];
}

// Find the average velocity (speed and direction) of the other boids and
// adjust velocity slightly to match.
function matchNeighbourVelocities(boid) {
  let neighbours = boidsCloserThan(boid, boid.visualRange);

  if (neighbours.length) {
    let sum_neighbour_velocities = add_list_of_vectors(neighbours.map(b => b.velocity));
    let average_neighbour_velocity = scale_vector(sum_neighbour_velocities, 1 / neighbours.length);
    let speed_difference = substract_vectors(average_neighbour_velocity, boid.velocity);
    return speed_difference;
  }
  return [0,0];
}

function airFriction(boid) {
  const speedLimit = 10;
  let currentSpeed = magnitude(boid.velocity)
  if (currentSpeed > speedLimit) {
    return scale_vector(boid.velocity, speedLimit - currentSpeed);
  } else {
    return [0,0];
  }
}

function adjustAcceleration(boid, direction, factor) {
  boid.acceleration = add_vectors(
    boid.acceleration,
    scale_vector(
      direction, factor
    )
  );
}

function decide_where_to_fly(boid) {

  // factor, force
  forces = [
    [boid.centeringFactor, flyTowardsCenterOfNeighbours(boid) ],
    [boid.avoidBoidsFactor    , avoidCollisions(boid)              ],
    [boid.matchingFactor , matchNeighbourVelocities(boid)     ],
    [boid.avoidBorderFactor     , avoidWindowBorder(boid)            ],
    [0.5                 , airFriction(boid)                  ]
  ]
  let sum_forces = add_list_of_vectors(
    forces.map(tuple =>
      scale_vector(tuple[1], tuple[0])
    )
  );

  boid.acceleration = scale_vector(sum_forces, 1/boid.mass); // F = m * a

}

function physics_step(boid){
  // Update the position, velocity
  boid.position = add_vectors(boid.position, boid.velocity);
  boid.velocity = add_vectors(boid.velocity, boid.acceleration);
  boid.age += 1;
  if (DRAW_TRAIL) {
    boid.history.push(boid.position);
    boid.history = boid.history.slice(-50);
  }
}

function collisions() {
  // remove colliding boids from array "boids" and return a new array of them
  let collided_boids = [];
  boids = boids.filter(
    function (boid, i) {
      if (distanceToBoarders(boid) < 0) {
        // collision with boarder
        collided_boids.push(boid);
        return false
      } else if (boids.some((otherBoid, j) => distance(boid, otherBoid) < COLLISION_DISTANCE && i != j)) {
        // collision
        collided_boids.push(boid);
        return false
      } else {
        // no collision
        return true;
      }
    }
  )
  return collided_boids
}

// DRAWING ON THE CANVAS

function redraw(boids, collided_boids) {
  const ctx = document.getElementById("boids").getContext("2d");
  ctx.clearRect(0, 0, width, height);

  drawSoftBorder(ctx);
  for (let boid of collided_boids) {
    drawCollision(ctx, boid.position);
  }
  for (let boid of boids) {
    drawBoid(ctx, boid);
  }
}

function drawBoid(ctx, boid) {
  const angle = Math.atan2(boid.velocity[1], boid.velocity[0]);
  ctx.translate(boid.position[0], boid.position[1]);
  ctx.rotate(angle);
  ctx.translate(-boid.position[0], -boid.position[1]);

  // boid body: Triangle
  let ageFactor = 1 + Math.min(2, boid.age / 1000) // between 1 and 3
  let speedFactor = 1 + Math.min(2, magnitude(boid.velocity) / 10) // between 1 and 3
  ctx.beginPath();
  ctx.moveTo(boid.position[0], boid.position[1]);
  ctx.lineTo(boid.position[0] - 15 * speedFactor, boid.position[1] + 5 * ageFactor);
  ctx.lineTo(boid.position[0] - 15 * speedFactor, boid.position[1] - 5 * ageFactor);
  ctx.lineTo(boid.position[0], boid.position[1]);
  ctx.fillStyle = BOID_COLOR;
  ctx.fill();

  ctx.setTransform(1, 0, 0, 1, 0, 0);

  if (DRAW_TRAIL) {
    ctx.beginPath();
    ctx.moveTo(boid.history[0][0], boid.history[0][1]);
    for (const point of boid.history) {
      ctx.lineTo(point[0], point[1]);
    }
    ctx.strokeStyle = TRAIL_COLOR;
    ctx.stroke();
  }

  if (DRAW_VISUAL_RANGE) {
    ctx.beginPath();
    ctx.arc(boid.position[0], boid.position[1], boid.visualRange, 0, 2 * Math.PI, false);
    ctx.strokeStyle = VISUAL_RANGE_COLOR;
    ctx.stroke();
  }

  if (DRAW_MIN_DISTANCE) {
    ctx.beginPath();
    ctx.arc(boid.position[0], boid.position[1], boid.minDistance, 0, 2 * Math.PI, false);
    ctx.strokeStyle = MIN_DISTANCE_COLOR;
    ctx.stroke();
  }

}

function drawSoftBorder(ctx){
  ctx.beginPath();
  ctx.moveTo(MARGIN   , MARGIN);
  ctx.lineTo(width - MARGIN, MARGIN);
  ctx.lineTo(width - MARGIN, height - MARGIN);
  ctx.lineTo(MARGIN, height - MARGIN);
  ctx.lineTo(MARGIN, MARGIN);
  ctx.strokeStyle = SOFTBORDER_COLOR;
  ctx.stroke();
}

function drawCollision(ctx, position){
  const radius = 15;
  ctx.beginPath();
  ctx.arc(position[0], position[1], radius, 0, 2 * Math.PI, false);
  ctx.fillStyle = COLLISION_COLOR;
  ctx.fill();
}

// STATISTICS

function collectStatistics() {
  PROPERTIES_TO_TRACK.forEach(property => {
    let property_extractor = (boid) => boid[property];
    if (property.endsWith("magnitude")) {
      property_extractor = (boid) => magnitude(boid[property.replace(/ magnitude/, '')]);
    }

    let values = boids.map(property_extractor);
    statistics[property] = summarizeListOfNumbers(values);
  });
}

function summarizeListOfNumbers (values) {
  const mean = values.reduce((a, b) => a + b) / values.length;
  const std_derivation = Math.sqrt(values.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / values.length);
  return [mean, std_derivation, Math.min(...values), Math.max(...values)];
}

function logStatistics() {
  console.log(
    [
      `global age: ${statistics.globalAge} deaths: ${statistics.allDeaths}`
    ].concat(
      PROPERTIES_TO_TRACK.map(
        property => `${property}:`.padEnd(25, ' ')
          + `${statistics[property][0].toFixed(4)}`.padStart(10, ' ')
          + ' +- '
          + `${statistics[property][1].toFixed(4)}`.padStart(8, ' ')
          + ' between '
          + `${statistics[property][2].toFixed(4)}`.padStart(8, ' ')
          + ' and '
          + `${statistics[property][3].toFixed(4)}`.padStart(8, ' ')
      )
    ).join('\n')
  );
}

// Functions that act on the whole canvas

// Called initially and whenever the window resizes to update the canvas
// size and width/height variables.
function sizeCanvas() {
  const canvas = document.getElementById("boids");
  width = window.innerWidth;
  height = window.innerHeight;
  canvas.width = width;
  canvas.height = height;
}

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

// Main animation loop
function animationLoop() {

  // Update each boid
  for (let boid of boids) {
    // Update the velocities according to each rule
    decide_where_to_fly(boid);
    physics_step(boid);
  }

  let collided_boids = collisions();
  statistics.allDeaths += collided_boids.length;
  statistics.globalAge += 1;

  // Clear the canvas and redraw all the boids in their current positions
  redraw(boids, collided_boids)

  repopulate();

  collectStatistics();
  logStatistics();

  // sleep(100);

  if (play) {
    // Schedule the next frame
    window.requestAnimationFrame(animationLoop);
  }
}

function playSingleFrame() {
  play = false;
  window.requestAnimationFrame(animationLoop);
}

function pauseOrResume() {
  if (play) {
    play = false;
  } else {
    play = true;
    window.requestAnimationFrame(animationLoop);
  }
}

function keypressHandler(event) {
  if (event.key === 'p' || event.key === ' ') {
    pauseOrResume();
  } else if (event.key === '.') {
    playSingleFrame();
  }
}

window.addEventListener('keypress', keypressHandler);

window.onload = () => {
  // Make sure the canvas always fills the whole window
  window.addEventListener("resize", sizeCanvas, false);
  sizeCanvas();

  // Randomly distribute the boids to start
  repopulate(random=true);

  // Schedule the main animation loop
  window.requestAnimationFrame(animationLoop);
};
